import Vue from 'vue';
import App from './App.vue';
import './assets/index.css';
Vue.config.productionTip = false;

import { extend } from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';
extend('email', email);
extend('required', {
  ...required,
  message: 'This field is required'
});
new Vue({
  render: h => h(App)
}).$mount('#app');
