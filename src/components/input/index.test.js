import { mount } from '@vue/test-utils';
import Input from './index.vue';
import { extend } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';
extend('required', {
  ...required
});

describe('The input component', () => {
  let wrapper = null;

  beforeEach(() => {
    wrapper = mount(Input);
  });
  it('Input should render the passed value', async () => {
    const textInput = wrapper.find('input[type="text"]');
    await textInput.setValue('some value');
    expect(wrapper.find('input[type="text"]').element.value).toBe('some value');
  });

  it('Value should be emitted to the parent component after on change', async () => {
    wrapper.vm.$emit('foo');

    await wrapper.vm.$nextTick();
    expect(wrapper.emitted().foo).toBeTruthy();
  });
});
