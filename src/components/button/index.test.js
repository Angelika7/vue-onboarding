import { mount } from '@vue/test-utils';
import Button from './index.vue';

describe('The button component', () => {
  let wrapper = null;
  let elButton = null;

  beforeEach(() => {
    wrapper = mount(Button, {
      propsData: {
        id: 'button test'
      }
    });
    elButton = wrapper.find('button');
  });

  it('The button should be rendered correctly', () => {
    expect(wrapper.element).toMatchSnapshot();
  });

  it('The button component should trigger primaryStyles method if variant is primary', async () => {
    await wrapper.setProps({ variant: 'primary' });
    const primaryClass = ['bg-primary-blue text-white text-base'];
    const allClasses = elButton.classes();
    expect(allClasses.every(i => primaryClass.includes(i)));
  });

  it('The button component should trigger secondaryStyles method if variant is secondary', async () => {
    await wrapper.setProps({ variant: 'secondary' });
    const secondaryClass = [
      'bg-white text-primary-blue ring-primary-blue ring-2 text-base'
    ];
    const allClasses = elButton.classes();
    expect(allClasses.every(i => secondaryClass.includes(i)));
  });

  it('The button component should contains class opacity-40 when prop isDisabled is true', async () => {
    await wrapper.setProps({ isDisabled: true });
    expect(elButton.classes()).toContain('opacity-40');
  });

  it('The button component should contain small classes when size prop is small', async () => {
    const defaultClass = ['px-3.5 py-0.5 text-sm rounded-sm'];
    const stretchedClass = ['px-20 py-0.5 text-sm rounded-sm'];
    const allClasses = elButton.classes();

    await wrapper.setProps({ size: 'small' });
    expect(allClasses.every(i => defaultClass.includes(i)));

    await wrapper.setProps({ isStretched: true });
    expect(allClasses.every(i => stretchedClass.includes(i)));
  });

  it('The button component should contain medium classes when size prop is medium', async () => {
    const defaultClass = ['px-7 py-2.5 text-sm rounded'];
    const stretchedClass = ['px-20 py-2.5 text-sm rounded'];
    const allClasses = elButton.classes();

    await wrapper.setProps({ size: 'medium' });
    expect(allClasses.every(i => defaultClass.includes(i)));

    await wrapper.setProps({ isStretched: true });
    expect(allClasses.every(i => stretchedClass.includes(i)));
  });

  it('The button component should contain large classes when size prop is large', async () => {
    const defaultClass = ['px-9 py-3.5 text-base rounded-md'];
    const stretchedClass = ['px-24 py-3.5 text-base rounded-md'];
    const allClasses = elButton.classes();

    await wrapper.setProps({ size: 'large' });
    expect(allClasses.every(i => defaultClass.includes(i)));

    await wrapper.setProps({ isStretched: true });
    expect(allClasses.every(i => stretchedClass.includes(i)));
  });
});
