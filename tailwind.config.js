const plugin = require('tailwindcss/plugin');
const checkedSiblingPlugin = plugin(function({ addVariant }) {
  addVariant('checked-sibling', ({ container }) => {
    container.walkRules(rule => {
      rule.selector = `:checked + .checked-sibling\\:${rule.selector.slice(1)}`;
    });
  });
});
module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  purge: false,
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      sm: '420px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px'
    },
    colors: {
      transparent: 'transparent',
      'primary-blue': '#0096CC',
      'primary-dark': '#233A80',
      'primary-medium': '#405466',
      'primary-light': '#BFE5F2',
      success: '#077426',
      'success-light': '#ECFEF1',
      warning: '#FEC90B',
      'warning-light': '#FDFF8C',
      error: '#CC0000',
      'error-light': '#F9E1E1',
      white: '#FFFFFF',
      surface: '#F8F8F8',
      'light-gray': '#EBEBEB',
      'light-gray02': '#E4E4E4',
      'medium-gray': '#8F8F8F',
      'dark-gray': '#707070',
      slate: '#525252',
      'dark-knight': '#161723',
      black: '#000'
    },
    extend: {
      scale: ['active', 'group-hover'],
      height: {
        '10vh': '10vh',
        '20vh': '20vh',
        '30vh': '30vh',
        '40vh': '40vh',
        '50vh': '50vh',
        '60vh': '60vh',
        '70vh': '70vh',
        '80vh': '80vh',
        '90vh': '90vh'
      },
      maxHeight: {
        '10vh': '10vh',
        '20vh': '20vh',
        '30vh': '30vh',
        '40vh': '40vh',
        '50vh': '50vh',
        '60vh': '60vh',
        '70vh': '70vh',
        '80vh': '80vh',
        '90vh': '90vh'
      },
      width: {
        '10vw': '10vw',
        '20vw': '20vw',
        '30vw': '30vw',
        '40vw': '40vw',
        '50vw': '50vw',
        '60vw': '60vw',
        '70vw': '70vw',
        '80vw': '80vw',
        '90vw': '90vw',
        '600px': '600px'
      },
      padding: {
        5.5: '22px'
      },
      opacity: {
        45: '.45'
      },
      borderRadius: {
        xs: '3px'
      },
      boxShadow: {
        button: '0 2px 4px 0 rgba(0, 0, 0, 0.16)',
        soft: '0 4px 4px 0 rgba(0, 0, 0, 0.16)'
      }
    }
  },
  variants: {
    borderColor: [
      'disabled',
      'responsive',
      'hover',
      'focus',
      'active',
      'group-hover'
    ],
    boxShadow: [
      'disabled',
      'responsive',
      'hover',
      'focus',
      'active',
      'group-hover'
    ],
    textColor: [
      'disabled',
      'responsive',
      'hover',
      'focus',
      'active',
      'group-hover'
    ],
    backgroundColor: [
      'disabled',
      'responsive',
      'hover',
      'focus',
      'active',
      'group-hover'
    ],
    whitespace: ['hover'],
    extend: {
      backgroundColor: ['disabled', 'checked-sibling', 'checked'],
      boxShadow: ['disabled', 'checked-sibling'],
      borderRadius: ['disabled', 'checked-sibling'],
      fontWeight: ['disabled', 'checked-sibling'],
      textColor: ['disabled', 'checked-sibling'],
      opacity: ['disabled'],
      cursor: ['disabled'],
      inset: ['checked'],
      borderColor: ['checked']
    }
  },
  plugins: [require('@tailwindcss/forms'), checkedSiblingPlugin]
};
